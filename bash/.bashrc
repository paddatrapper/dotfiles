#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

#### Aliases:
alias cls='clear && ls'
alias dquilt='quilt --quiltrc=${HOME}/.quiltrc-dpkg'
alias dropbox='~/bin/dropbox_uploader.sh'
alias gnomedb='journalctl /usr/bin/gnome-shell -f -o cat'
alias ldapvi='ldapvi -D uid=kyle,ou=people,dc=aims,dc=ac,dc=za -h ldaps://lemonldap.aims.ac.za --tls allow --discover'
alias ls='ls --color=auto'
alias octave='octave --no-gui'
alias ping='ping -p 54657272792050726174636865747420'
alias scenebuilder='SceneBuilder'
alias teamviewer='~/bin/teamviewer.sh'
alias today='date +"%A, %B &d, %Y"'
if [ -f /usr/bin/weechat ]; then
    alias weechat='weechat --dir ~/.weechat'
else
    alias weechat='ssh -t cloud "export LC_CTYPE=en_ZA.UTF-8; tmux attach -t weechat"'
fi

# Error tolerant:
alias cd..='cd ..'

# Turn off the system beep
if [ -f /usr/bin/xset ]; then
    xset b off
fi

# Enable autocomplete for dquilt alias
complete -F _quilt_completion $_quilt_completion_opt dquilt

export JAVA_HOME=/usr

# User specific environment and startup programs
export PATH=$PATH:$HOME/bin
export PATH=$PATH:/usr/games
export EDITOR=vim

# Android configuration
export PATH=$PATH:/opt/android-sdk/tools:/opt/android-sdk/platform-tools

# Python configuration
export PATH=~/.local/bin:$PATH

# Arduino makefile config
export ARDUINO_DIR=/usr/share/arduino
export ARDMK_DIR=/usr/share/arduino
export AVR_TOOLS_DIR=/usr

# Ignore commands starting with a space or if it duplicates the command before 
# it
export HISTCONTROL=ignoreboth
export HISTSIZE=300000
export HISTFILESIZE=300000

# Sets the sudo editor to vim
export SUDO_EDITOR="vim"

# Enables CCache for building Android
export USE_CCACHE=1

# Sets Debian packaging options
export DEBFULLNAME="Kyle Robbertze"
export DEBEMAIL="paddatrapper@debian.org"

# GPG Signing
export GPG_TTY=`tty`

# Sets ansible options
export ANSIBLE_INVENTORY=~/.ansible/hosts

# Sets Docbook stylesheet location
export DOCBOOK_STYLESHEETS=/usr/share/xml/docbook/stylesheet/docbook-xsl

# Sets askpass variable
if [ -f /usr/bin/ksshaskpass ]; then
    export SSH_ASKPASS="/usr/bin/ksshaskpass"
fi

if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent > ~/.ssh-agent-thing
fi
if [[ "$SSH_AGENT_PID" == "" ]]; then
    eval "$(<~/.ssh-agent-thing)" > /dev/null
fi

if [ -f /usr/share/powerline/bindings/bash/powerline.sh ]; then
    powerline-daemon -q
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
    . /usr/share/powerline/bindings/bash/powerline.sh
elif [ -f /usr/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh ]; then
    powerline-daemon -q
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
    . /usr/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh
elif [ -f ~/.local/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh ]; then
    powerline-daemon -q
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
    . ~/.local/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh
fi

# Set up Google cloud
export GOOGLE_APPLICATION_CREDENTIALS=~/.kube/kubernetes-gke-account.json

# Change go package path
export GOPATH=~/projects/.go
export PATH=$GOPATH/bin:$PATH
