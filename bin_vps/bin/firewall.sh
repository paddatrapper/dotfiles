#!/bin/bash
#
# iptables configuration script
#

# Check for root
#if [ '$(id -u)' != '0' ]; then
#	echo "Root required to modify iptables"
#	exit -1
#fi

#
# Flush all current rules from iptables
#
 iptables -F
 iptables -X
#
# Allow SSH connections 
# This is essential when working on remote servers via SSH to prevent locking yourself out of the system
#
 iptables -A INPUT -p tcp --dport 22 -j ACCEPT -m comment --comment "SSH"

#
# Set default policies for INPUT, FORWARD and OUTPUT chains
#
 iptables -P INPUT DROP
 iptables -P FORWARD DROP
 iptables -P OUTPUT ACCEPT 

#
# Set access for localhost
#
 iptables -A INPUT -i lo -j ACCEPT

#
# INPUT Policies
#

# Accept packets belonging to established and related connection
 iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# Accept MySQL connection
 iptables -A INPUT -p tcp --dport 3306 -j ACCEPT -m comment --comment "MySQL"

# Accept Postgresql connection
 iptables -A INPUT -p tcp --dport 5432 -j ACCEPT -m comment --comment "Postgresql"

# Accept Web server connection
 iptables -A INPUT -p tcp --dport 80 -j ACCEPT -m comment --comment "Web server"

# Accept darkhttpd connection
 iptables -A INPUT -p tcp --dport 81 -j ACCEPT -m comment --comment "darkhttpd"

# Accept Shell-in-a-box connection
 iptables -A INPUT -p tcp --dport 443 -j ACCEPT -m comment --comment "Shellinabox"

# Accept Tomcat connection
 iptables -A INPUT -p tcp --dport 8080 -j ACCEPT -m comment --comment "Tomcat"

# Accept weechat connection
 iptables -A INPUT -p tcp --dport 9001 -j ACCEPT -m comment --comment "weechat"

# Accept VPN connection
 iptables -A INPUT -p udp --dport 1194 -j ACCEPT -m comment --comment "VPN"
 iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE

# Accept Chat connection
 iptables -A INPUT -p tcp --dport 3000 -j ACCEPT -m comment --comment "Chat"

#
#
# Save settings
#
 /sbin/service iptables save

#
# List rules
#
 iptables -nvL
 exit 0
