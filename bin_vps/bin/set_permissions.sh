#!/bin/bash
#
#Sets directory permissions to 755 and files to 664
#

FILE_PERMISSIONS=664
DIRECTORY_PERMISSIONS=755

function change_directory {
	for file in $1/*
	do
		if [[ -f $file ]]; then
			echo "Setting $file to $FILE_PERMISSIONS"
			chmod $FILE_PERMISSIONS $file
		else if [[ -d $file ]]; then
			echo "Setting $file to $DIRECTORY_PERMISSIONS"
			chmod $DIRECTORY_PERMISSIONS $file
			echo "Entering $file"
			change_directory $file
		fi
		fi
	done
}

change_directory $1
exit 0
