#!/bin/bash
#
# Fetches CaseTracker from github and publishes the site
#

mkdir ~/.temp
cd ~/.temp
git clone https://github.com/paddatrapper/CaseTracker.git

cd CaseTracker
mvn site

rm -r /var/www/kritsit.ddns.net/public_html/casetracker/*
mkdir -p /var/www/kritsit.ddns.net/public_html/casetracker/{server,client}

cd target/site
cp -r ./* /var/www/kritsit.ddns.net/public_html/casetracker/

cd ../../client/target/site
cp -r ./* /var/www/kritsit.ddns.net/public_html/casetracker/client/

cd ../../../server/target/site
cp -r ./* /var/www/kritsit.ddns.net/public_html/casetracker/server/

rm -rf ~/.temp
