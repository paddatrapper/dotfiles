#!/usr/bin/env python3
import argparse
import glob
import os
import yaml

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert a directory of public keys into a valid yaml list of users (from the file names) and keys to be used in the DebConf ansible roles')
    parser.add_argument('directory', help='The directory of public keys to use')
    args = parser.parse_args()

    pub_keys = []
    for filepath in glob.iglob(os.path.join(args.directory, '*.pub')):
        with open(filepath, 'r') as fp:
            name = os.path.basename(filepath).split('.')[0]
            ssh_keys = []
            for line in fp:
                ssh_keys.append(line.strip())

            pub_keys.append({
                'user': name,
                'keys': ssh_keys
            })
    print(yaml.dump({'ssh_public_keys': pub_keys}))
