#!/bin/bash

FILES_ALL="$1"
FILES_KEEP="$2"

if [ ! -d $FILES_ALL ]; then
	echo 'Unedited directory not found not found'
	exit 1
fi
if [ ! -d $FILES_KEEP ]; then
	echo 'Edited directory not found'
	exit 2
fi

FILES_ALL="$FILES_ALL/*.JPG"
FILES_KEEP="$FILES_KEEP/*.JPG"

for f in $FILES_ALL
do
	file="$(dirname $FILES_KEEP)/$(basename $f .JPG).jpg"
	if [ ! -f $file ]; then
		rm $f
		rm "$f.xmp"
	fi
done
exit
