#!/bin/bash
#
# Moves all files in directories further down in the tree to be in the working 
# directory
#

FILE=""

function move_up 
{
	mv $FILE/* ./
}

for f in $(find . -mindepth 1 -type d -printf "%f\n")
do
	FILE=f
	if [ -d "$f" ]; then
		echo "$f"
		#move_up
	fi
done
exit
