import os

directory = "/home/kyle/uct-radio/rundowns/songs"
for filename in os.listdir(directory):
    new_filename = filename.replace(",", "").replace("\u2019", "").replace("feat.", "feat").replace("ft.", "ft").replace("&", "").replace("__", "_")
    os.rename(os.path.join(directory, filename), os.path.join(directory, new_filename))
