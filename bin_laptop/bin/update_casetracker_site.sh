#!/bin/bash
#
# Updates the site documentation on http://vps/casetracker/
#
# Create updated site documentation
# Remove old files from server
echo "Removing old site files"
ssh kyle@vps "rm -rf /var/www/kritsit.ddns.net/public_html/casetracker; mkdir -p /var/www/kritsit.ddns.net/public_html/casetracker/{client,server};"

# Copy new files to server
echo "Copying new project site from developer"
scp -r ./target/site/* kyle@vps:/var/www/kritsit.ddns.net/public_html/casetracker/

echo "Copying new client site from developer"
scp -r ./client/target/site/* kyle@vps:/var/www/kritsit.ddns.net/public_html/casetracker/client/

echo "Copying new server site from developer"
scp -r ./server/target/site/* kyle@vps:/var/www/kritsit.ddns.net/public_html/casetracker/server/
