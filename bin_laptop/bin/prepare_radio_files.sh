#!/bin/bash

#
# Creates a mp3 version of ./*.ogg files in a subdirectory 'upload'
#

mkdir upload

for track in ./*.ogg
do
	ffmpeg -i "$track" -map_metadata 0:s:0 "upload/`basename "$track" .ogg`.mp3"
done
rm upload/*_Raw.mp3
