# Functions
if status is-interactive
    alias cls='clear && ls'
    alias ping='ping -p 54657272792050726174636865747420'
    alias fd='fdfind'
    alias vsphere-lab='vsphere-lab-env | source'
    alias teamviewer="teamviewer-wrapper"
    alias clearlyenergy-production="source ~/bin/clearlyenergy-production"
end

# Powerline
if status is-interactive
    set fish_function_path $fish_function_path "/usr/share/powerline/bindings/fish"
    powerline-setup
end

# Turn off the system beep
if [ -f /usr/bin/xset ]
    xset b off 2> /dev/null
end

# User specific environment and startup programs
fish_add_path -P $HOME/bin
fish_add_path -P /usr/games
fish_add_path -P /usr/local/bin
if status is-interactive
    set -gx EDITOR vim
    set -gx SUDO_EDITOR vim
end

# Python configuration
fish_add_path -P $HOME/.local/bin

# Arduino makefile config
set -gx ARDUINO_DIR /usr/share/arduino
set -gx ARDMK_DIR /usr/share/arduino
set -gx AVR_TOOLS_DIR /usr

# Sets Debian packaging options
set -gx DEBFULLNAME "Kyle Robbertze"
set -gx DEBEMAIL "paddatrapper@debian.org"

# Sets Docbook stylesheet location
set -gx DOCBOOK_STYLESHEETS /usr/share/xml/docbook/stylesheet/docbook-xsl

# Sets askpass variable
#if [ -f /usr/bin/ksshaskpass ]
#    set -gx SSH_ASKPASS "/usr/bin/ksshaskpass"
#end

if status is-interactive
    if ! pgrep -u "$USER" ssh-agent > /dev/null
        eval (ssh-agent -c > ~/.ssh-agent-pid)
    end
    if ! set -q SSH_AGENT_PID
        eval (cat ~/.ssh-agent-pid) > /dev/null
    end
end

# Disable fish MotD
if status is-interactive
    set fish_greeting
end

# Set up Golang
set -gx GOPATH ~/.go
fish_add_path -P $GOPATH/bin

# Set up franciscolourenco/done
set -U __done_min_cmd_duration 600000 # 60
set -U __done_exclude 'git (?!push|pull)|vim|tmux|ssh|man'
set -U __done_sway_ignore_visible 1
set -U __done_notification_urgency_level low
set -U __done_notification_urgency_level_failure normal
