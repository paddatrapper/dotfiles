" Use Vim settings, rather than Vi settings
set nocompatible

" Enable file type detection.
filetype plugin indent on

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" Fuzzy autocomplete of commands
set wildmenu

" Sets the map leader to ','
let mapleader=","

" Do not use mode lines
" https://github.com/numirias/security/blob/master/doc/2019-06-04_ace-vim-neovim.md
set nomodeline

" Set tab/spaces
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

" See the difference between the current buffer and the file it was loaded from
if !exists(":DiffOrig")
      command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                        \ | wincmd p | diffthis
endif

" Undo and Backup
set nobackup
set nowb
set noswapfile
set undofile
set undolevels=1000
set undoreload=1000
set undodir=~/.vim/tmp/undo/
set history=1000    " keep 1000 lines of command line history

" UI Layout
" Show the cursor position all the time
set ruler
"
" Display incomplete commands
set showcmd
set splitbelow
set splitright

" Shows line numbering
set relativenumber
set number

set mouse=a

" When editing a file, always jump to the last known cursor position.
autocmd BufReadPost *
                  \ if line("'\"") > 1 && line("'\"") <= line("$") |
                  \   exe "normal! g`\"" |
                  \ endif

" Remaps escape to jj
inoremap jj <ESC>

" Remaps moving to visual lines
nnoremap j gj
nnoremap k gk

" Saves the current vim layout to be reloaded later
nnoremap <leader>s :mksession<CR>

" Colour column 80 and 120
nnoremap <leader>z :set colorcolumn=80,120<CR>
nnoremap <leader>x :set colorcolumn=<CR>

" Searching
" Do incremental searching
set incsearch
set smartcase

" Highlight syntax and search
syntax on
set hlsearch

" Clears search highlights
noremap <leader><space> :nohlsearch<CR>

" Folding
set foldenable
set foldlevelstart=10
set foldnestmax=10
nnoremap <space> za
set foldmethod=indent

" LaTeX
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

" c.vim
let g:C_CplusCFlags = '-std=c++11 -Wall -g -O0 -c'
let g:C_CplusLFlags = '-std=c++11 -Wall -g -O0'
let g:C_MapLeader = 'z'

" YouCompleteMe
packadd! youcompleteme
let g:ycm_error_symbol = 'E>'
let g:ycm_warning_symbol = 'W>'
let g:ycm_extra_conf_globlist = ['~/projects/*']
let g:ycm_filetype_blacklist = {
                  \ 'text' : 1,
                  \ 'tex' : 1,
                  \ 'markdown' : 1,
                  \ 'rst': 1
                  \ }

" Syntax Checking
packadd! ale
let g:ale_fix_on_save = 0
let g:ale_fixers = {
      \   '*': ['remove_trailing_lines', 'trim_whitespace'],
      \   'python': ['black', 'autoflake', 'autoimport', 'isort'],
      \   'terraform': ['terraform'],
      \   'yaml': ['yamlfix']
      \ }
let g:ale_python_auto_pipenv = 1
let g:ale_virtualtext_cursor = 'disabled'
let g:ale_virtualenv_dir_names = ['.env', '.venv', 'env', 've-py3', 've', 'virtualenv', 'venv', 'pyenv']

" Powerline
if has('python3')
      python3 from powerline.vim import setup as powerline_setup
      python3 powerline_setup()
      python3 del powerline_setup
elseif has('python')
      python from powerline.vim import setup as powerline_setup
      python powerline_setup()
      python del powerline_setup
else
      echoe 'No python support detected'
endif
set laststatus=2

" Highlight trailing whitespace
highlight BadWhitespace ctermbg=red guibg=red
match BadWhitespace /\s\+$/

" Grammarous
nmap <leader>gc :GrammarousCheck<CR>
nmap <leader>gr :GrammarousReset<CR>

let g:grammarous#hooks = {}
function! g:grammarous#hooks.on_check(errs) abort
      nmap <leader>gn <Plug>(grammarous-move-to-next-error)
      nmap <leader>gb <Plug>(grammarous-move-to-previous-error)
      nmap <leader>go <Plug>(grammarous-open-info-window)
      nmap <leader>gq <Plug>(grammarous-close-info-window)
      nmap <leader>gf <Plug>(grammarous-fixit)
      nmap <leader>gd <Plug>(grammarous-disable-rule)
endfunction

function! g:grammarous#hooks.on_reset(errs) abort
      nunmap <leader>gn
      nunmap <leader>gb
      nunmap <leader>go
      nunmap <leader>gq
      nunmap <leader>gf
      nunmap <leader>gd
endfunction

" FZF
source /usr/share/doc/fzf/examples/fzf.vim

colorscheme catppuccin_mocha
set nocursorline
" Make background transparent
hi Normal ctermbg=NONE guibg=NONE
hi NonText ctermbg=NONE guibg=NONE

" Redraw the window on context changed
au FocusGained * :redraw!

" Tagbar configuration
packadd! tagbar
nmap <leader>t :TagbarOpen j<CR>
let g:tagbar_type_go = {
                  \ 'ctagstype' : 'go',
                  \ 'kinds'     : [
                  \ 'p:package',
                  \ 'i:imports:1',
                  \ 'c:constants',
                  \ 'v:variables',
                  \ 't:types',
                  \ 'n:interfaces',
                  \ 'w:fields',
                  \ 'e:embedded',
                  \ 'm:methods',
                  \ 'r:constructor',
                  \ 'f:functions'
                  \ ],
                  \ 'sro' : '.',
                  \ 'kind2scope' : {
                  \ 't' : 'ctype',
                  \ 'n' : 'ntype'
                  \ },
                  \ 'scope2kind' : {
                  \ 'ctype' : 't',
                  \ 'ntype' : 'n'
                  \ },
                  \ 'ctagsbin'  : 'gotags',
                  \ 'ctagsargs' : '-sort -silent'
                  \ }

" vim:foldmethod=marker:foldlevel=0
