" Set tab
setlocal tabstop=2
setlocal shiftwidth=2
setlocal expandtab
" vim: set ts=8 sts=4 sw=4 expandtab :
