" Wrap words at 80 characters
setlocal textwidth=80
setlocal formatoptions+=t
setlocal formatoptions-=l
setlocal spell spelllang=en_gb
