" Java specific settings
setlocal tabstop=4
setlocal shiftwidth=4
setlocal expandtab

" Set DelimitMate options
let b:delimitMate_matchpairs = "(:),{:},[:],<:>"
let b:delimitMate_quotes = "\" '"
let b:delimitMate_expand_cr = 1
