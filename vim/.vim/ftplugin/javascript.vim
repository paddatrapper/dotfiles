" JavaScript specific settings

" Set DelimitMate options
let b:delimitMate_matchpairs = "(:),{:},[:],<:>"
let b:delimitMate_quotes = "\" '"
let b:delimitMate_expand_cr = 1

" Set tab
setlocal tabstop=2
setlocal shiftwidth=2
setlocal expandtab
